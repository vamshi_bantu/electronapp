package com.kp.hca.electronApp.test;

import org.testng.annotations.Test;

import com.kp.hca.electronApp.pages.HomePage;
import com.kp.hca.electronApp.pages.WelcomePage;
import com.qmetry.qaf.automation.data.MetaData;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;

public class HomeTestCases extends WebDriverTestCase {

	@Test
	@MetaData(value = "{'groups':['Home']}")
	public void signInVerification() {
		
		WelcomePage welcomePage = new WelcomePage();
		welcomePage.invoke();
		
		HomePage homePage = new HomePage();
		homePage.headerKPLogoText.isDisplayed();
		homePage.headerHealthCareAnywhere.isDisplayed();
		homePage.headerProfileIcon.isDisplayed();
		homePage.headerUsername.verifyText("Nancy X. Nissen");
		homePage.headerDialpad.isDisplayed();
		homePage.headerSignOut.isDisplayed();
		homePage.headerSignOut.click();
		
		welcomePage.linkSignIn.isDisplayed();
		
	}
	
	@Test
	@MetaData(value = "{'groups':['Home']}")
	public void footerTextVerification() {
		
		WelcomePage welcomePage = new WelcomePage();
		welcomePage.invoke();
		
		HomePage homePage = new HomePage();
		homePage.navHome.isDisplayed();
		homePage.navInvitation.isDisplayed();
		homePage.navGroup.isDisplayed();
		homePage.navFavorites.isDisplayed();
		homePage.navHistory.isDisplayed();
		homePage.navHelp.isDisplayed();
		homePage.pageContentKPLogoText.isDisplayed();
		homePage.pageContentHealthCareAnywhere.isDisplayed();
		homePage.mapImage.isDisplayed();
		homePage.footer.verifyText("© 2020 Kaiser Foundation Health Plan, Inc.");
	}
}
