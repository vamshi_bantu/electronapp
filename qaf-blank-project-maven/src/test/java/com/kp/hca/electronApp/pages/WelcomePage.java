package com.kp.hca.electronApp.pages;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class WelcomePage extends WebDriverBaseTestPage<WebDriverTestPage> {

	
	@Override
    protected void openPage(PageLocator arg0, Object... arg1) {
        driver.get("/");
    }
	
	@FindBy(locator = "//a[@class='Sign-In']")
    public QAFWebElement linkSignIn;
	
	@FindBy(locator="//div[@class='login_header_logo']")
	public QAFWebElement headerLogoKaiserPermanente;
	
	@FindBy(locator = "//div[contains(text(), 'HealthCare Anywhere')]")
	public QAFWebElement headerHealthCareAnywhere;
	
	@FindBy(locator="//div[@class='Login_kpLogo']")
	public QAFWebElement logoKaiserPermanente;
	
	@FindBy(locator="//span[text()='HealthCare']")
	public QAFWebElement textHealthCare;
	
	@FindBy(locator="//div[@class='map-image']")
	public QAFWebElement mapImage;
	
	@FindBy(locator="//div[contains(text(), 'Kaiser Foundation Health Plan, Inc.')]")
	public QAFWebElement textCopyright;
	
	@FindBy(locator="//h2[contains(text(), 'Welcome to Health Care Anywhere')]")
	public QAFWebElement headerWelcomeToHealthCareAnywhere;
	
	@FindBy(locator="//h3[contains(text(), 'Please select env to continue')]")
	public QAFWebElement headerPleaseSelectEnvToContinue;
	
	@FindBy(locator="//a[contains(@href, 'dev')]")
	public QAFWebElement buttonDEV;
	
	@FindBy(locator="//a[contains(@href, 'qa')]")
	public QAFWebElement buttonQA;
	
	@FindBy(locator="//a[contains(@href, 'perf')]")
	public QAFWebElement buttonPERF;
	
	@FindBy(locator="//a[contains(@href, 'preprod')]")
	public QAFWebElement buttonPREPROD;
	
	@FindBy(locator="//a[contains(@href, 'videovisit.kp.org')]")
	public QAFWebElement buttonPROD;
	
	@FindBy(locator="//div[@class='corp_logo']")
	public QAFWebElement logoKPSignOn;
	
	@FindBy(locator="//div[contains(text(), 'Please sign on using your desktop credentials')]")
	public QAFWebElement textPleaseSignOn;
	
	@FindBy(locator="//input[@id='username']")
	public QAFWebElement textBoxUsername;
	
	@FindBy(locator="//input[@id='password']")
	public QAFWebElement textBoxPassword;
	
	@FindBy(locator="//a[contains(text(),'Forgot NUID')]/@href")
	public QAFWebElement linkForgetNUID;
	
	@FindBy(locator="//a[contains(text(),'Forgot Password')]/@href")
	public QAFWebElement linkForgotPassword;

	@FindBy(locator="//a[@id='submitLinkID']")
	public QAFWebElement buttonSignOn;
	
	
	public void invoke() {
		headerHealthCareAnywhere.isDisplayed();
		linkSignIn.click();
		buttonQA.click();
		textBoxUsername.sendKeys("K254593");
		textBoxPassword.sendKeys("ecc12345$");
		buttonSignOn.click();
		
	}
	
}
