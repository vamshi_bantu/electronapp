package com.kp.hca.electronApp.pages;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebComponent;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class InvitationPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	
	
	@FindBy(locator = "//a[contains(@href, 'invitation')]")
    public QAFWebElement invitation;
	
	@FindBy(locator="//h1[@class='heading']")
	public QAFWebElement textSendAnInvitation;
	
	@FindBy(locator = "//*[@id='invitation-type']")
	public QAFWebElement selectInvitationType;
	
	@FindBy(locator="//option[@value='GUEST_PROVIDER']")
	public QAFWebElement dropDownGUESTPROVIDER;
	
	@FindBy(locator="//option[@value='PATIENT']")
	public QAFWebElement dropDownPatient;
	
	@FindBy(locator="//option[@value='FACILITY']")
	public QAFWebElement dropDownFacility;
	
	@FindBy(locator="//option[@value='PROVIDER']")
	public QAFWebElement dropDownProvider;
	
	@FindBy(locator = "//*[@id='name']")
	public QAFWebElement textboxGuestProviderName;
	
	@FindBy(locator= "//div[@class='hca-radio-button-group form-check']//input[@id='email']")
	public QAFWebElement radioButtonSendInviteToEmail;
	
	@FindBy(locator= "//div[@class='hca-radio-button-group form-check']//input[@id='sms']")
	public QAFWebElement radioButtonSendInviteToMobile;
	
	@FindBy(locator= "//div[@class='form-group']//input[@id='email']")
	public QAFWebElement textboxInviteToEmail;
	
	@FindBy(locator= "//div[@id='hca-radio-button-group form-check']//input[@id='sms'] | //*[@id=\"mobileNumber\"]")
	public QAFWebElement textboxInviteToMobile;
	
	@FindBy(locator= "//input[@id='patientMRN']")
	public QAFWebElement textboxPatientMRN;
	
	@FindBy(locator= "//input[@id='firstName']")
	public QAFWebElement textboxPatientFristName;
	
	@FindBy(locator= "//input[@id='lastName']")
	public QAFWebElement textboxPatientLastName;
	
	@FindBy(locator= "//button[@type='submit']")
	public QAFWebElement buttonSendInvite;
	
	@FindBy(locator="//div[@class='modal-content']")
	public QAFWebComponent InvitationPopUp;
	
	@FindBy(locator="//button[contains(text(), 'Invite')]")
	public QAFWebElement buttonInvite;
	
	
	@Override
	public void waitForPageToLoad() {
		
		textSendAnInvitation.isDisplayed();
		super.waitForPageToLoad();
	}
	
	
	
	
	
	@Override
    protected void openPage(PageLocator arg0, Object... arg1) {
        driver.get("/");
    }
	
	
}
