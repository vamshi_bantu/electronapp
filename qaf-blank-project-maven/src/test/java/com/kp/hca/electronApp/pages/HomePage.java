package com.kp.hca.electronApp.pages;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class HomePage extends WebDriverBaseTestPage<WebDriverTestPage> {
	
	@Override
    protected void openPage(PageLocator arg0, Object... arg1) {
        driver.get("/");
    }
	
	@FindBy(locator="//div[@class='logo']//img[@class='mobile-hide']")
	public QAFWebElement headerKPLogoText;
	
	@FindBy(locator="//div[@class='logo']//span[contains(text(),'HealthCare')]")
	public QAFWebElement headerHealthCareAnywhere;
	
	@FindBy(locator="//div[@class='hca-member-container']//i[@class='hca-icon icon-userprofile']")
	public QAFWebElement headerProfileIcon;
	
	@FindBy(locator="//div[@class='hca-member-container']//span[@class='hca-username']")
	public QAFWebElement headerUsername;
	
	@FindBy(locator="//button[@class='hca-iconbutton']")
	public QAFWebElement headerDialpad;
	
	@FindBy(locator="//div[@class='popover-inner']")
	public QAFWebElement dialPadExpand;
	
	@FindBy(locator="//button[@class='hca-signout-button']")
	public QAFWebElement headerSignOut;
	
	@FindBy(locator="//div[@class='collapse navbar-collapse']//a[@href='/healthcareanywhere/provider']")
	public QAFWebElement navHome;
	
	@FindBy(locator="//a[contains(@href, 'invitation')]")
	public QAFWebElement navInvitation;
	
	@FindBy(locator="//a[contains(@href, 'group')]")
	public QAFWebElement navGroup;
	
	@FindBy(locator="//a[contains(@href, 'favorites')]")
	public QAFWebElement navFavorites;
	
	@FindBy(locator="//a[contains(@href, 'history')]")
	public QAFWebElement navHistory;
	
	@FindBy(locator="//a[contains(@href, 'help')]")
	public QAFWebElement navHelp;
	
	@FindBy(locator="//div[@class='home-container']/img")
	public QAFWebElement pageContentKPLogoText;
	
	@FindBy(locator="//div[@class='home-container']//div[@class='appName']")
	public QAFWebElement pageContentHealthCareAnywhere;
	
	@FindBy(locator="//div[@class='background-image']")
	public QAFWebElement mapImage;
	
	@FindBy(locator="//footer[@id='kprcFooter']")
	public QAFWebElement footer;
	
	
}
