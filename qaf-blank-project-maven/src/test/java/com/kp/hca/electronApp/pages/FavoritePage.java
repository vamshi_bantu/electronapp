package com.kp.hca.electronApp.pages;

import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class FavoritePage {
	
	@FindBy(locator = "//a[@class='nav-link active']")
    public QAFWebElement favorite;

	@FindBy(locator ="//*[@id='hca-favorites-type']")
	public QAFWebElement ViewFavoriteType;

}
