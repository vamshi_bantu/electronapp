package com.kp.hca.electronApp.pages;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class VideoVisitPage extends WebDriverBaseTestPage<WebDriverTestPage> {
	
	
	@FindBy(locator="//img[@class='logo kpicon-desktop']")
	public QAFWebElement headerKPLogo;
	
	@FindBy(locator="//h1[contains(text(),'Video Visits')]")
	public QAFWebElement headerVideoVisits;
	
	@FindBy(locator="//div[@class='videoControlsPanel']//i[@aria-label='Microphone Off']")
	public QAFWebElement buttonMute;
	
	@FindBy(locator="//div[@class='videoControlsPanel']//i[@aria-label='Microphone On']")
	public QAFWebElement buttonUnMute;
	
	@FindBy(locator="//div[@class='videoControlsPanel']//i[@aria-label='Camera Off']")
	public QAFWebElement buttonVideoOn;
	
	@FindBy(locator="//div[@class='videoControlsPanel']//i[@aria-label='Camera On']")
	public QAFWebElement buttonVideoOff;
	
	@FindBy(locator="//div[@class='videoControlsPanel']//i[@aria-label='Share']")
	public QAFWebElement buttonShareScreen;
	
	@FindBy(locator="//div[@class='videoControlsPanel']//i[@aria-label='End Call']")
	public QAFWebElement buttonEndCall;
	
	@FindBy(locator="//div[@class='hostSpan']")
	public QAFWebElement hostName;
	
	@FindBy(locator="//div[@class='selfview toggleview']//div[@class='selfview-toggle']")
	public QAFWebElement selfVideoTrayOpen;
	
	@FindBy(locator="//div[@class='selfview toggleview close-selfview']//div[@class='selfview-toggle']")
	public QAFWebElement selfVideoTrayClose;
	
	@FindBy(locator="//button[@id='participants-btn']")
	public QAFWebElement participantsListButton;
	
	@FindBy(locator="//button[@id='chat-btn']")
	public QAFWebElement chatWindowButton;
	
	@FindBy(locator="//button[@id='device-management-btn']")
	public QAFWebElement deviceManagementButton;
	
	@FindBy(locator="//button[@id='help-btn']")
	public QAFWebElement helpSectionButton;
	
	@FindBy(locator="//div[@class='modal-header']")
	public QAFWebElement endVVPopUpHeader;
	
	@FindBy(locator="//div[@class='modal-content']//p[contains(text(),'If you leave, the visit will end.')]")
	public QAFWebElement endVVPopUpContent;
	
	@FindBy(locator="//div[@class='modal-content']//button[contains(text(),'End')]")
	public QAFWebElement endVVPopUpEndButton;
	
	@FindBy(locator="//div[@class='modal-content']//button[contains(text(),'Cancel')]")
	public QAFWebElement endVVPopUpCancelButton;
	
	@FindBy(locator="//div[@class='modal-container']//button[@aria-label='Close']")
	public QAFWebElement endVVPopUpCloseButton;
	
	@Override
	public void waitForPageToLoad() {
		
		hostName.isDisplayed();
		super.waitForPageToLoad();
	}
	
	@Override
    protected void openPage(PageLocator arg0, Object... arg1) {
        driver.get("/");
    }

}
