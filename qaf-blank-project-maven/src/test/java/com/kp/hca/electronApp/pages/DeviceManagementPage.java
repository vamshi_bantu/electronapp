package com.kp.hca.electronApp.pages;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class DeviceManagementPage extends WebDriverBaseTestPage<WebDriverTestPage> {
	
	@FindBy(locator="")
	public QAFWebElement headerKPVideoVisits;
	
	@FindBy(locator="//h2[@class='initial-join-header']")
	public QAFWebElement headerWelcome;
	
	@FindBy(locator="//div[@class='initial-join']//button[contains(text(),'Don')]")
	public QAFWebElement buttonDontConnect;
	
	
	@FindBy(locator="//button[contains(text(), 'OK')]")
	public QAFWebElement buttonOK;
	
	
	@Override
	public void waitForPageToLoad() {
		try {
			Thread.sleep(11000);
			driver.switchTo().frame("ivvrAppIFrame");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		super.waitForPageToLoad();
	}
	
	
	@Override
    protected void openPage(PageLocator arg0, Object... arg1) {
        driver.get("/");
    }

}
