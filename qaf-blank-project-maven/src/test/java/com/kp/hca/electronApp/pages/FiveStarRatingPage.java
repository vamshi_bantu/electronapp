package com.kp.hca.electronApp.pages;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class FiveStarRatingPage extends WebDriverBaseTestPage<WebDriverTestPage>{
	
	@FindBy(locator = "//div[@class='survey-container']//h2[contains(text(),'Please rate your video visit call')]")
    public QAFWebElement fiveStarHeader;
	
	@FindBy(locator = "//div[@class='star-rating']//i[@class='ivv-icon icon-star-not-selected']")
    public QAFWebElement fiveStarButton;
	
	@FindBy(locator = "//div[@class='survey-btn-wrapper']//label[contains(text(),'Audio')]")
    public QAFWebElement feedbackAudioButton;
	
	@FindBy(locator = "//div[@class='survey-btn-wrapper']//label[contains(text(),'Video')]")
    public QAFWebElement feedbackVideoButton;
	
	@FindBy(locator = "//div[@class='survey-btn-wrapper']//label[contains(text(),'Ease')]")
    public QAFWebElement feedbackEaseOfUseButton;
	
	@FindBy(locator = "//textarea[@class='additional-text-box']")
    public QAFWebElement feedbackTextBox;
	
	@FindBy(locator = "//button[@id='btn-submit']")
    public QAFWebElement fiveStarSubmit;
	
	@FindBy(locator = "//button[@class='icon-options-button skip-text'] | //button[text() ='Skip']")
    public QAFWebElement fiveStarSkip;
	
	
	@Override
	public void waitForPageToLoad() {
		
		fiveStarHeader.isDisplayed();
		super.waitForPageToLoad();
	}

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}

	

    
}
