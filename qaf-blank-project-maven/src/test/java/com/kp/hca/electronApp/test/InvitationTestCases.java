package com.kp.hca.electronApp.test;

import org.testng.annotations.Test;

import com.kp.hca.electronApp.pages.DeviceManagementPage;
import com.kp.hca.electronApp.pages.FiveStarRatingPage;
import com.kp.hca.electronApp.pages.HomePage;
import com.kp.hca.electronApp.pages.InvitationPage;
import com.kp.hca.electronApp.pages.VideoVisitPage;
import com.kp.hca.electronApp.pages.WelcomePage;
import com.qmetry.qaf.automation.data.MetaData;

public class InvitationTestCases {

	@Test
	@MetaData(value = "{'groups':['Invitation']}")
	public void inviteGuestProviderEmail() {

		WelcomePage welcomePage = new WelcomePage();
		welcomePage.invoke();
	
		HomePage homePage = new HomePage();
		homePage.navInvitation.click();
		
		InvitationPage invitationPage = new InvitationPage();
		invitationPage.waitForPageToLoad();
		invitationPage.dropDownGUESTPROVIDER.click();
		invitationPage.textboxGuestProviderName.sendKeys("Test");
		invitationPage.radioButtonSendInviteToEmail.click();
		invitationPage.textboxInviteToEmail.sendKeys("test@kp.org");
		invitationPage.buttonSendInvite.click();
		invitationPage.buttonInvite.click();
		
		DeviceManagementPage deviceManagementPage = new DeviceManagementPage();
		deviceManagementPage.waitForPageToLoad();
		deviceManagementPage.buttonOK.click();
		
		VideoVisitPage videoVisitPage = new VideoVisitPage();
		videoVisitPage.waitForPageToLoad();
		videoVisitPage.buttonEndCall.click();
		videoVisitPage.endVVPopUpEndButton.click();
		
		FiveStarRatingPage fiveStarRatingPage = new FiveStarRatingPage();
		fiveStarRatingPage.waitForPageToLoad();
		fiveStarRatingPage.fiveStarSkip.click();
		
		
	}
	
	@Test
	@MetaData(value = "{'groups':['Invitation']}")
	public void inviteGuestProviderSMS() {

		WelcomePage welcomePage = new WelcomePage();
		welcomePage.invoke();
	
		HomePage homePage = new HomePage();
		homePage.navInvitation.click();
		
		InvitationPage invitationPage = new InvitationPage();
		invitationPage.waitForPageToLoad();
		invitationPage.dropDownGUESTPROVIDER.click();
		invitationPage.textboxGuestProviderName.sendKeys("Test");
		invitationPage.radioButtonSendInviteToMobile.click();
		invitationPage.textboxInviteToMobile.sendKeys("1234567890");
		invitationPage.buttonSendInvite.click();
		invitationPage.buttonInvite.click();
		
		DeviceManagementPage deviceManagementPage = new DeviceManagementPage();
		deviceManagementPage.waitForPageToLoad();
		deviceManagementPage.buttonOK.click();
		
		VideoVisitPage videoVisitPage = new VideoVisitPage();
		videoVisitPage.waitForPageToLoad();
		videoVisitPage.buttonEndCall.click();
		videoVisitPage.endVVPopUpEndButton.click();
		
		FiveStarRatingPage fiveStarRatingPage = new FiveStarRatingPage();
		fiveStarRatingPage.waitForPageToLoad();
		fiveStarRatingPage.fiveStarSkip.click();
		
		
	}
	
	@Test
	@MetaData(value = "{'groups':['Invitation']}")
	public void invitePatientEmail() {

		WelcomePage welcomePage = new WelcomePage();
		welcomePage.invoke();
	
		HomePage homePage = new HomePage();
		homePage.navInvitation.click();
		
		InvitationPage invitationPage = new InvitationPage();
		invitationPage.waitForPageToLoad();
		invitationPage.dropDownPatient.click();
		invitationPage.textboxPatientFristName.sendKeys("First");
		invitationPage.textboxPatientLastName.sendKeys("Last");
		invitationPage.radioButtonSendInviteToEmail.click();
		invitationPage.textboxInviteToEmail.sendKeys("test@kp.org");
		invitationPage.buttonSendInvite.click();
		invitationPage.buttonInvite.click();
		
		DeviceManagementPage deviceManagementPage = new DeviceManagementPage();
		deviceManagementPage.waitForPageToLoad();
		deviceManagementPage.buttonOK.click();
		
		
		VideoVisitPage videoVisitPage = new VideoVisitPage();
		videoVisitPage.waitForPageToLoad();
		videoVisitPage.buttonEndCall.click();
		videoVisitPage.endVVPopUpEndButton.click();
		
		FiveStarRatingPage fiveStarRatingPage = new FiveStarRatingPage();
		fiveStarRatingPage.waitForPageToLoad();
		fiveStarRatingPage.fiveStarSkip.click();
		
	}
	
	@Test
	@MetaData(value = "{'groups':['Invitation']}")
	public void invitePatientSMS() {

		WelcomePage welcomePage = new WelcomePage();
		welcomePage.invoke();
	
		HomePage homePage = new HomePage();
		homePage.navInvitation.click();
		
		InvitationPage invitationPage = new InvitationPage();
		invitationPage.waitForPageToLoad();
		invitationPage.dropDownPatient.click();
		invitationPage.textboxPatientFristName.sendKeys("First");
		invitationPage.textboxPatientLastName.sendKeys("Last");
		invitationPage.radioButtonSendInviteToMobile.click();
		invitationPage.textboxInviteToMobile.sendKeys("1234567890");
		invitationPage.buttonSendInvite.click();
		invitationPage.buttonInvite.click();
		
		DeviceManagementPage deviceManagementPage = new DeviceManagementPage();
		deviceManagementPage.waitForPageToLoad();
		deviceManagementPage.buttonOK.click();
		
		
		VideoVisitPage videoVisitPage = new VideoVisitPage();
		videoVisitPage.waitForPageToLoad();
		videoVisitPage.buttonEndCall.click();
		videoVisitPage.endVVPopUpEndButton.click();
		
		FiveStarRatingPage fiveStarRatingPage = new FiveStarRatingPage();
		fiveStarRatingPage.waitForPageToLoad();
		fiveStarRatingPage.fiveStarSkip.click();
		
	}
}
